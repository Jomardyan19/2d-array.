﻿using System;

namespace ConsoleApp60
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Creat and Print Array
            int[,] arr = new int[5, 5];
            Random rnd = new Random();

            int n = arr.GetLength(0);
            int m = arr.GetLength(1);
           
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    arr[i, j] = rnd.Next(10, 100);
                    Console.Write(arr[i, j] + "  ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            #endregion

            int max = arr[0, 0];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write(arr[i, j] + "  ");
                    if (max < arr[i, j])
                        max = arr[i, j];
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("Max of these numbers is " + max);

        }
        
    }
}
